package com.example.paulj.mobile_assignment;


/**
 * Created by paul martin on 05/03/2018.
 * This class is used to return an image resource relating to the rating value of a restaurant.
 */
public class RestRating {
    private int ratingValue = 0;
    private int ratingImg = 0;

    /* There are two static array variables of type int, which hold the resource locations for both the marker images and the rating images.
       They are static variables because if they are changed they will need to be changed at class level and not object level. */
    public static int[] ratingMarkerImages = {R.drawable.map_marker_0, R.drawable.map_marker_1, R.drawable.map_marker_2,
            R.drawable.map_marker_3, R.drawable.map_marker_4, R.drawable.map_marker_5};
    public static int[] ratingImages = {R.drawable.rating_0, R.drawable.rating_1, R.drawable.rating_2, R.drawable.rating_3, R.drawable.rating_4, R.drawable.rating_5};


    /**
     * The RestRating constructor constructs a RestRating object with a default value of 0.
     */
    public RestRating() {
        this.ratingValue = 0;
    }

    /**
     * Individually sets the rating value of the object.
     *
     * @param ratingValue Returns the ratingValue of the object.
     */
    public void setRatingValue(int ratingValue) {
        this.ratingValue = ratingValue;
    }

    /**
     * Returns the rating value of the object.
     *
     * @return Returns the rating value of the object.
     */
    public int getRatingValue() {
        return this.ratingValue;
    }


    /**
     * This method takes in the rating value and runs through a block of if statements, checking if the rating value is is equal to a value from 0 to 5 inclusive.
     * If one of the if statements returns true, the value within the ratingMarkerImages array at that index is passed into the ratingImg int variable.
     * The ratingImg variable is then returned.
     *
     * @param ratingValue Takes in the rating value.
     * @return Returns the specific marker image.
     */
    public int ratingMarkerImg(int ratingValue) {
        this.ratingValue = ratingValue;

        if (this.ratingValue == 0) {
            /* As the rating value is 0 the image resource at index 0 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 0 will be accessed. */
            this.ratingImg = ratingMarkerImages[0];
        }
        if (this.ratingValue == 1) {
            /* As the rating value is 1 the image resource at index 1 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 1 will be accessed. */
            this.ratingImg = ratingMarkerImages[1];
        }
        if (this.ratingValue == 2) {
            /* As the rating value is 2 the image resource at index 2 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 2 will be accessed. */
            this.ratingImg = ratingMarkerImages[2];
        }
        if (this.ratingValue == 3) {
            /* As the rating value is 3 the image resource at index 3 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 3 will be accessed. */
            this.ratingImg = ratingMarkerImages[3];
        }
        if (this.ratingValue == 4) {
            /* As the rating value is 4 the image resource at index 4 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 4 will be accessed. */
            this.ratingImg = ratingMarkerImages[4];
        }
        if (this.ratingValue == 5) {
            /* As the rating value is 5 the image resource at index 5 of the ratingMarkerImages array is passed to the ratingImg variable.
               This means that the marker with the number 5 will be accessed. */
            this.ratingImg = ratingMarkerImages[5];
        }
        // returns the image resource.
        return ratingImg;
    }


    /**
     * This method takes in the rating value and runs through a block of if statements, checking if the rating value is is equal to a value from 0 to 5 inclusive.
     * If one of the if statements returns true, the value within the ratingImages array at that index is passed into the ratingImg int variable.
     * The ratingImg variable is then returned.
     *
     * @param ratingValue Takes in the rating value.
     * @return Returns the specific rating image.
     */
    public int ratingImg(int ratingValue) {
        this.ratingValue = ratingValue;

        if (this.ratingValue == 0) {
             /* As the rating value is 0 the image resource at index 0 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 0 will be accessed. */
            this.ratingImg = ratingImages[0];
        }
        if (this.ratingValue == 1) {
            /* As the rating value is 1 the image resource at index 1 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 1 will be accessed. */
            this.ratingImg = ratingImages[1];
        }
        if (this.ratingValue == 2) {
            /* As the rating value is 2 the image resource at index 2 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 2 will be accessed. */
            this.ratingImg = ratingImages[2];
        }
        if (this.ratingValue == 3) {
            /* As the rating value is 3 the image resource at index 3 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 3 will be accessed. */
            this.ratingImg = ratingImages[3];
        }
        if (this.ratingValue == 4) {
            /* As the rating value is 4 the image resource at index 4 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 4 will be accessed. */
            this.ratingImg = ratingImages[4];
        }
        if (this.ratingValue == 5) {
            /* As the rating value is 5 the image resource at index 5 of the ratingImages array is passed to the ratingImg variable.
               This means that the marker with the number 5 will be accessed. */
            this.ratingImg = ratingImages[5];
        }
        // returns the image resource.
        return ratingImg;
    }

}