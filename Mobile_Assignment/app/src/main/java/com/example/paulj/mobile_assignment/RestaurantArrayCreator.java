package com.example.paulj.mobile_assignment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by paul martin on 10/03/2018.
 * This class runs on the background thread of the asyncTask.
 * It takes in the index value to depict the option the user searched by and the buffered reader object which holds the JSON array of data sent back from the server.
 */
public class RestaurantArrayCreator {

    private BufferedReader in = null;
    private int index = 0;
    ArrayList<Restaurant> arrList = new ArrayList<Restaurant>();
    String line = null;
    JSONObject jsonObj = null;
    JSONArray jsonArr = null;

    // Empty constructor.
    public RestaurantArrayCreator() {

    }


    /**
     * This method takes in a buffered reader object which holds the JSON array of JSON objects sent back from the server and the index value depicting the option the user searched by.
     * The method then runs though buffered reader and adds the contents to a JSON array object.
     * It then loops through the length of the JSON Array, adds the JSON object at the depicted index to a JSON object object.
     * The method then checks if the index value is three, if it is three,
     * it means that the user has searched by location and therefor the constructor which includes distance is used, if not the constructor which does not include distance is used.
     * The restaurant object is then constructed using the attributes of the JSON object at the current index and added to an array list of restaurants.
     * Once the loop has finished and all the restaurant objects have been created, the array list is returned.
     *
     * @param in    Takes in a buffered reader object, which holds the data pulled from the server.
     * @param index Takes in an int object which holds the index data.
     * @return Returns an array list of film objects.
     */
    public ArrayList<Restaurant> restArray(BufferedReader in, int index) {
        this.index = index;
        this.in = in;

        try {
            // Clears the array.
            arrList.clear();
            // Clears the line variable.
            line = "";
            // While the buffered reader object has more results the while loop continues. The current line of the buffered reader is passed to the string variable called line.
            while ((line = in.readLine()) != null) {
                // The contents of line is passed into the JSON Array object.

                jsonArr = new JSONArray(line);
                // Loops through the length of the JSON array.
                for (int i = 0; i < jsonArr.length(); i++) {
                    // At every index, the object of that index is passed from the JSON array to a JSON object.
                    jsonObj = (JSONObject) jsonArr.get(i);
                    //  if index = 3 the body of this statement is called, otherwise the else statement is called.
                    if (index == 3) {
                        // A new restaurant object is constructed using the parameters of the JSON object.
                        // The rating value is parsed as an integer, so it can be compared within the RestRating class, to find the correct image.
                        // The distance value is also parsed as a double so it can be formatted within the restaurant class.
                        // Finally the restaurant object is added to the array list of restaurants.
                        Restaurant rest1 = new Restaurant(jsonObj.getString("BusinessName"), jsonObj.getString("AddressLine1"),
                                jsonObj.getString("AddressLine2"), jsonObj.getString("AddressLine3"), jsonObj.getString("PostCode"),
                                Integer.parseInt(jsonObj.getString("RatingValue")), jsonObj.getString("RatingDate"), jsonObj.getString("Longitude"),
                                jsonObj.getString("Latitude"), Double.parseDouble(jsonObj.getString("DistanceKM")));

                        arrList.add(rest1);


                    } else {
                        // A new restaurant object is constructed using the parameters of the JSON object.
                        // The rating value is parsed as an integer, so it can be compared within the RestRating class, to find the correct image.
                        // Finally the restaurant object is added to the array list of restaurants.
                        Restaurant rest1 = new Restaurant(jsonObj.getString("BusinessName"), jsonObj.getString("AddressLine1"),
                                jsonObj.getString("AddressLine2"), jsonObj.getString("AddressLine3"), jsonObj.getString("PostCode"),
                                Integer.parseInt(jsonObj.getString("RatingValue")), jsonObj.getString("RatingDate"), jsonObj.getString("Longitude"),
                                jsonObj.getString("Latitude"));

                        arrList.add(rest1);

                    }
                }
            }
            // Catching all possible exceptions that may be caused by the contents of the try block.
        } catch (MalformedURLException mUrlExcept) {
            mUrlExcept.printStackTrace();
        } catch (IOException ioExcept) {
            ioExcept.printStackTrace();
        } catch (JSONException jExcept) {
            jExcept.printStackTrace();
        }

        // Finally the array list of restaurant objects is returned.
        return arrList;
    }
}