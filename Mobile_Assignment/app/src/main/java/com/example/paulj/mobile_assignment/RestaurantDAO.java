package com.example.paulj.mobile_assignment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * Created by paul martin on 01/03/2018.
 * This class is used to connect to the server, pull back an array of JSON objects to an input stream reader, which is then pulled into a buffered reader.
 * The buffered reader is then returned. The class also implements Serializable, so the objects can be serialised.
 */
public class RestaurantDAO implements Serializable {

    private String displayStr = null;
    private String latStr = null;
    private String lngStr = null;
    private HttpURLConnection urlConnection = null;
    private String displayStrEncoded = null;
    private String urlStr = null;
    private URL url = null;
    private InputStreamReader ins = null;
    private BufferedReader in = null;

    /**
     * This method is used to search the server for restaurants by name.
     * It takes in the contents of the value entered by the user and adds the data onto the end of the URL string. The data is then pulled back and a buffered reader object is returned.
     *
     * @param displayStr The data entered into the edit text widget by the user.
     * @return Returns a buffered reader object.
     */
    public BufferedReader restByName(String displayStr) {
        // The contents of the user input is passed into the displayStr instance variable.
        this.displayStr = displayStr;

        try {
            /* The displayStr contents are encoding using the URLEncoder object in UTF-8 format and passed into the displayStrEncoded string.
               This converts all spaces into the + symbol, which is essential when adding the input data to the parameters in the URL string. */
            displayStrEncoded = URLEncoder.encode(this.displayStr, "UTF-8");
            /* The urlStr String is equal to the URL with the parameters for returning restaurants by name,
               the encoded string is also added to the end to search the server by the name entered by the user. */
            // The server which I connect to is supplied by my tutor Kris Welsh, the data is supplied by the council.
            urlStr = "http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_name&name=" + displayStrEncoded;
            // The full URL string is added into the URL constructor.
            url = new URL(urlStr);
            // The connection to the URL is opened.
            urlConnection = (HttpURLConnection) url.openConnection();
            // The input stream object pulls in the input stream from the URL.
            ins = new InputStreamReader(urlConnection.getInputStream());
            // The input stream reader is passed into the buffered reader.
            in = new BufferedReader(ins);

            // Catching all possible exceptions that may be caused by the contents of the try block.
        } catch (MalformedURLException mUrlExcept) {
            mUrlExcept.printStackTrace();
        } catch (IOException ioExcept) {
            ioExcept.printStackTrace();
        } finally {
            // Closes the connection to the URL to stop memory leaks.
            urlConnection.disconnect();
        }
        // Returns the buffered reader object.
        return in;
    }

    /**
     * This method is used to search the server for restaurants by post code.
     * It takes in the contents of the value entered by the user and adds the data onto the end of the URL string. The data is then pulled back and a buffered reader object is returned.
     *
     * @param displayStr The data entered into the edit text widget by the user.
     * @return Returns a buffered reader object.
     */
    public BufferedReader restByPostCode(String displayStr) {
        // The contents of the user input is passed into the displayStr instance variable.
        this.displayStr = displayStr;
        try {

            /* The displayStr contents are encoding using the URLEncoder object in UTF-8 format and passed into the displayStrEncoded string.
               This converts all spaces into the + symbol, which is essential when adding the input data to the parameters in the URL string. */
            displayStrEncoded = URLEncoder.encode(this.displayStr, "UTF-8");
            /* The urlStr String is equal to the URL with the parameters for returning restaurants by post code,
               the encoded string is also added to the end to search the server by the post code entered by the user. */
            urlStr = "http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_postcode&postcode=" + displayStrEncoded;
            // The full URL string is added into the URL constructor.
            url = new URL(urlStr);
            // The connection to the URL is opened.
            urlConnection = (HttpURLConnection) url.openConnection();
            // The input stream object pulls in the input stream from the URL.
            ins = new InputStreamReader(urlConnection.getInputStream());
            // The input stream reader is passed into the buffered reader.
            in = new BufferedReader(ins);

            // Catching all possible exceptions that may be caused by the contents of the try block.
        } catch (MalformedURLException mUrlExcept) {
            mUrlExcept.printStackTrace();
        } catch (IOException ioExcept) {
            ioExcept.printStackTrace();
        } finally {
            // Closes the connection to the URL to stop memory leaks.
            urlConnection.disconnect();
        }
        // Returns the buffered reader object.
        return in;
    }


    /**
     * This method is used to search the server for restaurants by location.
     * It takes in the current longitude and latitude of the device and returns the 10 nearest restaurants in a buffered reader object.
     *
     * @param latStr Takes in the latitude of the current location.
     * @param lngStr Takes in the longitude of the current location.
     * @return Returns a buffered reader object.
     */
    public BufferedReader restByLocation(String latStr, String lngStr) {
        // The contents of the longitude and latitude variables are passed into the corresponding instance variables.
        this.latStr = latStr;
        this.lngStr = lngStr;
        try {

            // The latStr and lngStr strings containing the values for the current longitude and latitude of the device are converted to the double data format
            // and passed into the double variables of lat and long. This is because the server expects the values to be of type double.
            double lat = Double.parseDouble(this.latStr);
            double lng = Double.parseDouble(this.lngStr);

            /* The urlStr String is equal to the URL with the parameters for returning restaurants by longitude and latitude,
               the variables of lat and lng are added to the end of the URL to search the server using the devices current location */
            urlStr = "http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_loc&lat=" + lat + "&long=" + lng;
            // The full URL string is added into the URL constructor.
            url = new URL(urlStr);
            // The connection to the URL is opened.
            urlConnection = (HttpURLConnection) url.openConnection();
            // The input stream object pulls in the input stream from the URL.
            ins = new InputStreamReader(urlConnection.getInputStream());
            // The input stream reader is passed into the buffered reader.
            in = new BufferedReader(ins);

            // Catching all possible exceptions that may be caused by the contents of the try block.
        } catch (MalformedURLException mUrlExcept) {
            mUrlExcept.printStackTrace();
        } catch (IOException ioExcept) {
            ioExcept.printStackTrace();
        } finally {
            // Closes the connection to the URL to stop memory leaks.
            urlConnection.disconnect();
        }
        // Returns the buffered reader object.
        return in;
    }

    /**
     * This method is used to search the server for restaurants which have been recently rated and returns the 10 most recently rated restaurants in a buffered reader object.
     *
     * @return Returns a buffered reader object.
     */
    public BufferedReader restByRecent() {

        try {
            // The urlStr String is equal to the URL with the parameters for returning restaurants which have been recently rated. No user input is needed.
            urlStr = "http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_recent";
            // The full URL string is added into the URL constructor.
            url = new URL(urlStr);
            // The connection to the URL is opened.
            urlConnection = (HttpURLConnection) url.openConnection();
            // The input stream object pulls in the input stream from the URL.
            ins = new InputStreamReader(urlConnection.getInputStream());
            // The input stream reader is passed into the buffered reader.
            in = new BufferedReader(ins);

            // Catching all possible exceptions that may be caused by the contents of the try block.
        } catch (MalformedURLException mUrlExcept) {
            mUrlExcept.printStackTrace();
        } catch (IOException ioExcept) {
            ioExcept.printStackTrace();
        } finally {
            // Closes the connection to the URL to stop memory leaks.
            urlConnection.disconnect();
        }

        // Returns the buffered reader object.
        return in;
    }

}
