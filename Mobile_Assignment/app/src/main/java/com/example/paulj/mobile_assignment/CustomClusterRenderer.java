package com.example.paulj.mobile_assignment;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by paul martin on 06/03/2018.
 * This class extends the DefaultClusterRenderer and enables changes to the properties of the markers.
 */

public class CustomClusterRenderer extends DefaultClusterRenderer<MyGoogleMapMarkers> {

    /**
     * Constructs a new CustomClusterRenderer object, which takes in the context, the map object and the cluster manager object.
     *
     * @param context        takes in the context
     * @param map            Takes in a map object
     * @param clusterManager Takes in a cluster manager object
     */
    public CustomClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<MyGoogleMapMarkers> clusterManager) {
        super(context, map, clusterManager);
    }

    /**
     * This method is overridden and takes in a marker object and the marker options object.
     * The method then uses the icon, snippet and title methods to modify the marker options object.
     * The MyGoogleMapMarkers object passed into this method supplies the values which are passed into the parameters of the methods.
     *
     * @param markerItem    Takes in a MyGoogleMapMarkers object.
     * @param markerOptions Takes in a MarkerOptions object
     */

    @Override
    protected void onBeforeClusterItemRendered(MyGoogleMapMarkers markerItem,
                                               MarkerOptions markerOptions) {
        // The properties of the marker object are all assigned to the marker options object.
        markerOptions.icon(markerItem.getIcon());
        markerOptions.snippet(markerItem.getSnippet());
        markerOptions.title(markerItem.getTitle());
    }

}
