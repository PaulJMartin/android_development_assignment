package com.example.paulj.mobile_assignment;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;


/**
 * Created by paul martin on 06/03/2018.
 * This class is used to construct a new google maps marker object.
 */

public class MyGoogleMapMarkers implements ClusterItem {
    private final LatLng mapPosition;
    private final String mapTitle;
    private final String mapSnippet;
    private final BitmapDescriptor mapIcon;


    /**
     * Constructor for the MyGoogleMapMarkers object.
     *
     * @param lat     Takes in the latitude of the restaurant object.
     * @param lng     Takes in the longitude of the restaurant object.
     * @param title   Takes in the name of the restaurant object.
     * @param snippet Takes in the rating date of the restaurant object or any other requested information.
     * @param icon    Takes in the marker image resource to show the restaurant objects rating value.
     */
    public MyGoogleMapMarkers(double lat, double lng, String title, String snippet, BitmapDescriptor icon) {
        mapPosition = new LatLng(lat, lng);
        mapTitle = title;
        mapSnippet = snippet;
        mapIcon = icon;
    }


    /**
     * Method for returning the position of the marker.
     *
     * @return Returns the value of the mapPosition variable.
     */
    @Override
    public LatLng getPosition() {
        return mapPosition;
    }

    /**
     * Method for returning the name of the marker.
     *
     * @return Returns the value of the mapTitle variable.
     */
    @Override
    public String getTitle() {
        return mapTitle;
    }

    /**
     * Method for returning the snippet of the marker.
     *
     * @return Returns the value of the mapSnippet variable.
     */
    @Override
    public String getSnippet() {
        return mapSnippet;
    }

    /**
     * Method for returning the icon of the marker.
     *
     * @return Returns the value of the mapIcon variable.
     */
    public BitmapDescriptor getIcon() {
        return mapIcon;
    }
}