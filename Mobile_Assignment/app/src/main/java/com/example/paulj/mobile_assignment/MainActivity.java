package com.example.paulj.mobile_assignment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by paul martin on 01/02/2018.
 * This is the main activity class, it is the controller class for the applications main activity.
 */

public class MainActivity extends AppCompatActivity implements Serializable {


    EditText display;
    RadioButton r_Name;
    RadioButton r_PostCode;
    RadioButton r_MyLocation;
    RadioButton r_MostRecent;
    ProgressBar progressBar;
    Button mapButton;
    ArrayList<Restaurant> arrList = new ArrayList<Restaurant>();
    ConnectivityManager connMgr = null;
    NetworkInfo networkInfo = null;
    String displayStr = null;
    RestaurantDAO restDAO = new RestaurantDAO();
    BufferedReader in = null;
    double[] currentLocation = null;
    double lat;
    double lng;
    TableLayout tl;
    TextView tv1 = null;
    TextView tv2 = null;
    TextView tv3 = null;
    TextView tv4 = null;
    TextView tv5 = null;
    TextView tv6 = null;
    TextView tv7 = null;


    /**
     * This method runs when the application boots up.
     * It instances the UI widgets, checks the location permissions and if granted find the location of the device using GPSS.
     *
     * @param savedInstanceState Saves the instance state data in a bundle, so it can be used again if the user reopens the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assigning the EditText widget to a variable to accept text input.
        display = (EditText) this.findViewById(R.id.editText);
        // Assigning the radio buttons to variables to check which option is selected.
        r_Name = (RadioButton) this.findViewById(R.id.radioButton_Name);
        r_PostCode = (RadioButton) this.findViewById(R.id.radioButton_PostCode);
        r_MyLocation = (RadioButton) this.findViewById(R.id.radioButton_MyLocation);
        r_MostRecent = (RadioButton) this.findViewById(R.id.radioButton2_MostRecent);
        // Assigning the TableLayout widget to a variable. Used when posting data back to the UI.
        tl = (TableLayout) this.findViewById(R.id.tableLayout);
          /* Assigning the Map Button to a variable and setting the button to GONE when the application is loaded.
             This is to ensure the map button cannot be selected when the application is loaded and when search by location is not selected. */
        mapButton = (Button) this.findViewById(R.id.mapButton);
        mapButton.setVisibility(View.GONE);

          /* Checks to see if the user has given the application permission to access their location.
             If the user hasn't previously used the app, they will be asked for their permission. */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    "android.permission.ACCESS_FINE_LOCATION"}, 1);
        } else {
            // If the location permission has been granted, the application will locate the current longitude and latitude of the device using the Location Manager .
            LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    // The longitude and latitude is placed into an array to be passed to the map activity when the map button is selected.
                    currentLocation = new double[]{lat, lng};
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                }

                @Override
                public void onProviderDisabled(String s) {
                }
            });
        }
    }

    /**
     * This method is called when the user clicks on the search button.
     * The method checks which radio button has ben selected and if the device is connected to a network,
     * before sending the index value associated with the radio button option and the contents of the user input(display) to the asyncTask objects execute method.
     *
     * @param v This parameter is never used but is a requirement. It holds the information for the ID of the onClick.
     */

    public void searchButton_onClick(View v) {
        // Unless otherwise stated the map button visibility is set to GONE.
        mapButton.setVisibility(View.GONE);
        // The data placed within the Edit Text is pulled into the displayStr variable.
        displayStr = display.getText().toString().trim();

        // The Connectivity Manger is used here to check if the application is connected to a network before proceeding with the request.
        connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {


            /* If the app is connected to a network, the application will check which of the radio buttons have been selected and will run the contents of the if statement,
               which corresponds with that selection.
             */

            // An instance of the MyTask class is created, this class extends AsyncTask.
            MyAsyncTask task = new MyAsyncTask();

            if (r_Name.isChecked()) {
                    /* The instance of MyTask is executed with two parameters,
                      the first is the index value which corresponds to the particular option selection and the second is the contents of the displayStr variable. */
                task.execute("1", displayStr);
            }

            if (r_PostCode.isChecked()) {
                /* The instance of MyTask is executed with two parameters,
                      the first is the index value which corresponds to the particular option selection and the second is the contents of the displayStr variable. */
                task.execute("2", displayStr);
            }

            if (r_MyLocation.isChecked()) {
                /* The instance of MyTask is executed with three parameters,
                   the first is the index value which corresponds to the particular option selection and the second and third are the current values of the longitude and latitude. */
                task.execute("3", Double.toString(lat), Double.toString(lng));
            }

            if (r_MostRecent.isChecked()) {
                /* The instance of MyTask is executed with one parameter, this is the index value which corresponds to the particular option selection. */
                task.execute("4");
            }
        }
    }

    /**
     * This method is called when the map button is selected. This button will only be visible if the user searches by location.
     * Within this method a new intent is created which is used to start the Maps Activity.
     * The array list of restaurant objects and the device location data is sent over to the maps activity, when the activity is started.
     *
     * @param v This parameter is never used but is a requirement. It holds the information for the ID of the onClick.
     */

    public void mapButton_onClick(View v) {
        // An instance of the intent class is created which points towards the MapsActivity class.
        Intent intent = new Intent(this, MapsActivity.class);

        // The array list which holds all the restaurant objects is serialised and passed to the MapsActivity intent.
        intent.putExtra("REST_DATA", (Serializable) arrList);
        // The array which holds the data for the longitude and latitude is serialised and passed to the MapsActivity intent.
        intent.putExtra("CURRENT_LOCATION", currentLocation);

        // This method starts the intent which opens the maps activity.
        startActivity(intent);
    }

    /**
     * This method accepts an array list of restaurant objects and creates a table of restaurants using the data.
     * The table rows and text/image views are created in this method and the data is also formatted here.
     *
     * @param arrList Takes in an array list of restaurant objects.
     */

    public void viewCreator(ArrayList<Restaurant> arrList) {

        // An instance of the RestRating class is created, which is used to decide which rating logo is implemented, depending on the rating number.
        RestRating restRating = new RestRating();


        // This for loop loops through all the restaurant objects.
        for (int i = 0; i < arrList.size(); i++) {


            // Each time the loop runs the table rows are reinstated, this is to clear the previous contents.
            TableRow tr1 = new TableRow(this);
            TableRow tr2 = new TableRow(this);
            TableRow tr3 = new TableRow(this);
            TableRow tr4 = new TableRow(this);
            TableRow tr5 = new TableRow(this);
            TableRow tr6 = new TableRow(this);
            TableRow tr7 = new TableRow(this);

            // Each time the loop runs the image view and text views are reinstated, this is to clear the previous contents.
            tv1 = new TextView(this);
            tv2 = new TextView(this);
            ImageView img1 = new ImageView(this);
            tv3 = new TextView(this);
            tv4 = new TextView(this);
            tv5 = new TextView(this);
            tv6 = new TextView(this);
            tv7 = new TextView(this);


            // This section programmatically sets the properties of the first text view. The first text view is the name of the establishment.
            TableRow.LayoutParams layoutParamsBusinessName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            layoutParamsBusinessName.setMargins(0, 150, 0, 75);
            tv1.setLayoutParams(layoutParamsBusinessName);
            tv1.setGravity(Gravity.CENTER);
            tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
            tv1.setTextSize(22);
            tv1.setTextColor(Color.BLACK);

            // This section programmatically sets the properties of the second text view. The second text view is the address of the establishment.
            TableRow.LayoutParams layoutParamsAddress = new TableRow.LayoutParams();
            layoutParamsAddress.weight = (float) 0.6;
            layoutParamsAddress.width = 0;
            tv2.setLayoutParams(layoutParamsAddress);

            // This section programmatically sets the properties of the image view.
            TableRow.LayoutParams layoutParamsImages = new TableRow.LayoutParams(650, 300);
            layoutParamsImages.setMargins(5, 20, 5, 30);

            // This property aligns the text views to the center of their rows.
            tv2.setGravity(Gravity.CENTER);
            tv5.setGravity(Gravity.CENTER);
            tv6.setGravity(Gravity.CENTER);

            // The contents of the restaurant object is parsed into a series of string variables.
            String restName = arrList.get(i).getBusinessName();
            String restAddress = "Address: " + "\n" + arrList.get(i).getAddressLine1() + "\n" + arrList.get(i).getAddressLine2() + "\n"
                    + arrList.get(i).getAddressLine3() + "\n" + arrList.get(i).getPostCode();

            String restRatedDate = "Rated Date: " + "\n" + arrList.get(i).getRatingDate();


            /* This if statement check if the rating value of the restaurant is -1, if so the text view is set to "EXEMPT, the properties of the text view are set
               and the text view is added to the corresponding table row. */
            if (arrList.get(i).getRatingValue() == -1) {
                tv4.setText("EXEMPT");
                tv4.setTextColor(Color.BLACK);
                tv4.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                tr4.addView(tv4);
                tv4.setGravity(Gravity.CENTER);
            }
            // If the value is not -1, the image view is set to the image which corresponds with the rating value of the restaurant.
            else {
                img1.setImageResource(restRating.ratingImg(arrList.get(i).getRatingValue()));
                img1.setLayoutParams(layoutParamsImages);
                tr4.addView(img1);
            }

            /* If the user searched by location, an additional table row is added which shows the distance from the users current position, to the location of the restaurant.
               The text view is allocated properties and is added to its corresponding table row. */
            if (r_MyLocation.isChecked()) {
                String restDistance = "Distance: " + "\n" + arrList.get(i).getDistanceKM() + " KM";
                tv6.setText(restDistance);
                tv6.setTextColor(Color.BLACK);
                tr6.addView(tv6);
            }


            // The specified text views are set to the string values containing the restaurant information. The text colour is also set to black.
            tv1.setText(restName);
            tv2.setText(restAddress);
            tv5.setText(restRatedDate);
            tv1.setTextColor(Color.BLACK);
            tv2.setTextColor(Color.BLACK);
            tv5.setTextColor(Color.BLACK);


            // The text views are added to their corresponding table rows.
            tr1.addView(tv1);
            tr2.addView(tv2);
            tr3.addView(tv3);
            tr5.addView(tv5);
            tr7.addView(tv7);


            // The table rows are all aligned to the center of the screen horizontally.
            tr1.setGravity(Gravity.CENTER);
            tr2.setGravity(Gravity.CENTER);
            tr3.setGravity(Gravity.CENTER);
            tr4.setGravity(Gravity.CENTER);
            tr5.setGravity(Gravity.CENTER);
            tr6.setGravity(Gravity.CENTER);


            // The table rows are added to the table layout and formatted.
            tl.addView(tr1, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            tl.addView(tr2, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
            tl.addView(tr3, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            tl.addView(tr4, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            tl.addView(tr5, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            // If the user searches by location, the distance is added to the table and the map button becomes visible, so the user can search for the restaurants on the map.
            if (r_MyLocation.isChecked()) {
                tl.addView(tr6, new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                mapButton.setVisibility(View.VISIBLE);
            }
        }

    }

    /**
     * This class extends AsyncTask and overrides the methods included in this class.
     * It takes in 3 parameters, the first is the data sent to the async object and specifically the doInBackground method,
     * the second is the value used with the onProgressUpdate method and the third is the object type returned from the doInBackground method and passed into the onPostExecute method.
     * The doInBackground method body runs in the background thread and therefor does not disrupt the main UI thread.
     * The output of the doInBackground method is passed to the onPostExecute, so the data can be used to modify the UI.
     */
    class MyAsyncTask extends AsyncTask<String, Integer, ArrayList<Restaurant>> {


        // All methods of the AsyncTask class are overridden.

        /**
         * This method is called when the MyTask object is executed and runs before the doInBackground method runs.
         * It removed all the contents of the table layout and initialises the progress bar.
         */
        @Override
        protected void onPreExecute() {
            // When this method runs the previous results are removed from the screen to make way for the updated results.
            tl.removeAllViews();
            // The progress bar widget is assigned to the progress bar variable and is set to visible.
            progressBar = (ProgressBar) findViewById(R.id.progressBar1);
            progressBar.setVisibility(View.VISIBLE);
        }


        /**
         * This method is the main part of the asyncTask, as the contents of the method is ran on the background thread.
         * This enables the user to continue to use the UI and stops the UI from freezing while the method is running.
         * The array of strings entered into the parameter of the execute method is passed into the method.
         * From here the index passed in is checked and the relevant IF statement is ran, this calls the relevant method on the restDAO object which returns a buffered reader object.
         * The index value and the buffered reader object is then passed into the restArray method on the RestaurantArrayCreator object, which returns an array list of restaurant objects.
         * This array list of restaurant objects is then passed to the onPostExecute method.
         *
         * @param strings An array of strings, passed in when the MyAsyncTask is executed.
         * @return Returns an array list of restaurant objects.
         */

        @Override
        protected ArrayList<Restaurant> doInBackground(String... strings) {

            // This variable holds the index value which informs the method of which IF statement to run.
            int index = Integer.parseInt(strings[0]);

               /* Each IF statement holds a buffered reader variable which is equal to the data returned by the restDAO object.
                  The data differs depending on which method is called on the restDAO object. */
            if (index == 1) {
                //The method called returns the list of restaurants which correspond to the name entered.
                in = restDAO.restByName(strings[1]);
            }
            if (index == 2) {
                //The method called returns the list of restaurants which correspond to the postcode entered.
                in = restDAO.restByPostCode(strings[1]);
            }
            if (index == 3) {
                //The method called returns the list of restaurants which correspond to the current latitude and longitude.
                in = restDAO.restByLocation(strings[1], strings[2]);
            }
            if (index == 4) {
                //The method called returns the list of recently updated restaurants.
                in = restDAO.restByRecent();
            }

            // A RestaurantArrayCreator object is created.
            RestaurantArrayCreator rCreate = new RestaurantArrayCreator();
                /* The restArray method is called on the RestaurantArrayCreator object and
                the contents of the buffered reader and value of the index are passed into the method. An array list of restaurants are returned. */
            arrList = rCreate.restArray(in, index);
            // The array list is returned and passed to the onPostExecute method.
            return arrList;
        }

        /**
         * This method is called when the doInBackground method is complete.
         * It calls the viewCreator method found in the main activity class with the array list of restaurants as the parameter.
         * It also sets the edit text widget to empty and sets the progress bar visibility to gone.
         *
         * @param restaurants Takes in an array list of restaurant objects.
         */
        @Override
        protected void onPostExecute(ArrayList<Restaurant> restaurants) {
            // The viewCreator method is called and the array list is passed in as a parameter.
            viewCreator(arrList);
            // The EditText widget is cleared.
            display.setText("");
            // The progress bar is set to GONE to inform the user that the task is complete.
            progressBar.setVisibility(View.GONE);

        }
    }

}