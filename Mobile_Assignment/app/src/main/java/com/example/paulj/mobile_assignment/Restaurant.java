package com.example.paulj.mobile_assignment;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by paul martin on 28/02/2018.
 * This the restaurant base class, it is used to create new restaurant objects,
 * individually set the attributes of the restaurants as well as individually return the attributes of the restaurants
 */


public class Restaurant implements Serializable {
    private String BusinessName = null;
    private String AddressLine1 = null;
    private String AddressLine2 = null;
    private String AddressLine3 = null;
    private String PostCode = null;
    private int RatingValue = 0;
    private String RatingDate = null;
    private String Longitude = null;
    private String Latitude = null;
    private String DistanceKM = null;
    // This object rounds the number found within variables of the double data format to 3 decimal places. It also converts the values to String format.
    public static DecimalFormat dF = new DecimalFormat("0.000");


    /**
     * This is the primary constructor, this constructor is used when the user selects any option other than search by location.
     *
     * @param BusinessName Constructs the BusinessName of the restaurant object.
     * @param AddressLine1 Constructs the AddressLine1 of the restaurant object.
     * @param AddressLine2 Constructs the AddressLine2 of the restaurant object.
     * @param AddressLine3 Constructs the AddressLine3 of the restaurant object.
     * @param PostCode     Constructs the PostCode of the restaurant object.
     * @param RatingValue  Constructs the RatingValue of the restaurant object.
     * @param RatingDate   Constructs the RatingDate of the restaurant object.
     * @param Longitude    Constructs the Longitude of the restaurant object.
     * @param Latitude     Constructs the Latitude of the restaurant object.
     */
    public Restaurant(String BusinessName, String AddressLine1, String AddressLine2, String AddressLine3,
                      String PostCode, int RatingValue, String RatingDate, String Longitude, String Latitude) {
        this.BusinessName = BusinessName;
        this.AddressLine1 = AddressLine1;
        this.AddressLine2 = AddressLine2;
        this.AddressLine3 = AddressLine3;
        this.PostCode = PostCode;
        this.RatingValue = RatingValue;
        this.RatingDate = RatingDate;
        this.Longitude = Longitude;
        this.Latitude = Latitude;

    }

    /**
     * This constructor is only called when the user selects search by location.
     * This constructor is different as it also takes in a variable of distance and converts the value to 3 decimal places, which shows the distance to the nearest meter.
     *
     * @param BusinessName Constructs the BusinessName of the restaurant object.
     * @param AddressLine1 Constructs the AddressLine1 of the restaurant object.
     * @param AddressLine2 Constructs the AddressLine2 of the restaurant object.
     * @param AddressLine3 Constructs the AddressLine3 of the restaurant object.
     * @param PostCode     Constructs the PostCode of the restaurant object.
     * @param RatingValue  Constructs the RatingValue of the restaurant object.
     * @param RatingDate   Constructs the RatingDate of the restaurant object.
     * @param Longitude    Constructs the Longitude of the restaurant object.
     * @param Latitude     Constructs the Latitude of the restaurant object.
     * @param DistanceKM   Constructs the DistanceKM of the restaurant object.
     */
    public Restaurant(String BusinessName, String AddressLine1, String AddressLine2, String AddressLine3,
                      String PostCode, int RatingValue, String RatingDate, String Longitude, String Latitude, double DistanceKM) {
        this.BusinessName = BusinessName;
        this.AddressLine1 = AddressLine1;
        this.AddressLine2 = AddressLine2;
        this.AddressLine3 = AddressLine3;
        this.PostCode = PostCode;
        this.RatingValue = RatingValue;
        this.RatingDate = RatingDate;
        this.Longitude = Longitude;
        this.Latitude = Latitude;
        this.DistanceKM = dF.format(DistanceKM);
    }


    /**
     * Method for modifying the name of the restaurant object.
     *
     * @param BusinessName Accepts a string input from the user and sets the BusinessName variable to the new value.
     */
    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    /**
     * Method for returning the name of the restaurant object.
     *
     * @return Returns the value of the BusinessName variable.
     */
    public String getBusinessName() {
        return BusinessName;
    }


    /**
     * Method for modifying the 1st line of address of the restaurant object.
     *
     * @param AddressLine1 Accepts a string input from the user and sets the AddressLine1 variable to the new value.
     */
    public void setAddressLine1(String AddressLine1) {
        this.AddressLine1 = AddressLine1;
    }

    /**
     * Method for returning the 1st line of address of the restaurant object.
     *
     * @return Returns the value of the AddressLine1 variable.
     */
    public String getAddressLine1() {
        return AddressLine1;
    }


    /**
     * Method for modifying the 2nd line of address of the restaurant object.
     *
     * @param AddressLine2 Accepts a string input from the user and sets the AddressLine2 variable to the new value.
     */
    public void setAddressLine2(String AddressLine2) {
        this.AddressLine2 = AddressLine2;
    }

    /**
     * Method for returning the 2nd line of address of the restaurant object.
     *
     * @return Returns the value of the AddressLine2 variable.
     */
    public String getAddressLine2() {
        return AddressLine2;
    }


    /**
     * Method for modifying the 3rd line of address of the restaurant object.
     *
     * @param AddressLine3 Accepts a string input from the user and sets the AddressLine3 variable to the new value.
     */
    public void setAddressLine3(String AddressLine3) {
        this.AddressLine3 = AddressLine3;
    }

    /**
     * Method for returning the 3rd line of address of the restaurant object.
     *
     * @return Returns the value of the AddressLine3 variable.
     */
    public String getAddressLine3() {
        return AddressLine3;
    }

    /**
     * Method for modifying the post code of the restaurant object.
     *
     * @param PostCode Accepts a string input from the user and sets the PostCode variable to the new value.
     */
    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    /**
     * Method for returning the post code of the restaurant object.
     *
     * @return Returns the value of the PostCode variable.
     */
    public String getPostCode() {
        return PostCode;
    }


    /**
     * Method for modifying the rating value of the restaurant object.
     *
     * @param RatingValue Accepts a string input from the user and sets the RatingValue variable to the new value.
     */
    public void setRatingValue(int RatingValue) {
        this.RatingValue = RatingValue;
    }

    /**
     * Method for returning the rating value of the restaurant object.
     *
     * @return Returns the value of the RatingValue variable.
     */
    public int getRatingValue() {
        return RatingValue;
    }


    /**
     * Method for modifying the rating date of the restaurant object.
     *
     * @param RatingDate Accepts a string input from the user and sets the RatingDate variable to the new value.
     */
    public void setRatingDate(String RatingDate) {
        this.RatingDate = RatingDate;
    }

    /**
     * Method for returning the rating date of the restaurant object.
     *
     * @return Returns the value of the RatingDate variable.
     */
    public String getRatingDate() {
        return RatingDate;
    }


    /**
     * Method for modifying the longitude of the restaurant object.
     *
     * @param Longitude Accepts a string input from the user and sets the Longitude variable to the new value.
     */
    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    /**
     * Method for returning the longitude of the restaurant object.
     *
     * @return Returns the value of the Longitude variable.
     */
    public String getLongitude() {
        return Longitude;
    }


    /**
     * Method for modifying the Latitude of the restaurant object.
     *
     * @param Latitude Accepts a string input from the user and sets the Latitude variable to the new value.
     */
    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    /**
     * Method for returning the latitude of the restaurant object.
     *
     * @return Returns the value of the Latitude variable.
     */
    public String getLatitude() {
        return Latitude;
    }


    /**
     * Method for modifying the DistanceKM of the restaurant object to the nearest meter.
     *
     * @param DistanceKM Accepts a string input from the user and sets the DistanceKM variable to the new value.
     */
    public void setDistanceKM(double DistanceKM) {
        this.DistanceKM = dF.format(DistanceKM);
    }

    /**
     * Method for returning the restaurant distance in KM from the current location.
     *
     * @return Returns the value of the DistanceKM variable.
     */
    public String getDistanceKM() {
        return DistanceKM;
    }

}
