package com.example.paulj.mobile_assignment;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul martin on 10/02/2018.
 * This class holds all the methods for the map activity, it is the controller class for the applications maps activity.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ClusterManager<MyGoogleMapMarkers> mapClusterManager;
    ArrayList<Restaurant> arrList = null;
    BitmapDescriptor markerIcon = null;
    RestRating restRating = new RestRating();
    double lat = 0.00;
    double lng = 0.00;


    /**
     * This method is called when the map activity is loaded.
     * It instantiates the maps activity and map widget.
     *
     * @param savedInstanceState Saves the instance state data in a bundle, so it can be used again if the user reopens the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * This method is called when the map is ready.
     * It sets the map settings, pulls in the data from the main activity and calls the clusterSetUp method.
     *
     * @param googleMap Takes in a googleMap object.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // The GoogleMap object passed into the method is assigned to the google map private instance variable.
        mMap = googleMap;

        // By default the zoom button controls are set to false, for this application I have enabled the zoom controls for the purpose of the screencast.
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // The restaurant array list and the location array are received from the main activity, deserialized and loaded into the relevant variables.
        arrList = (ArrayList<Restaurant>) getIntent().getSerializableExtra("REST_DATA");
        double[] currentLocationStr = (double[]) getIntent().getSerializableExtra("CURRENT_LOCATION");

        // Sets the map camera to show the current location of the user .
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocationStr[0], currentLocationStr[1]), 14.5f));

        // Runs the setUpClusterer method.
        clusterSetUp();
        /* Finds the map and cluster manager object of this activity and passes the objects to a CustomClusterRenderer object.
           The CustomClusterRenderer finds the icon parameter of all of the marker objects and sets the the marker options object to the corresponding values.
           The cluster managers setRenderer method then uses the values to render the updated icons of the markers. */
        mapClusterManager.setRenderer(new CustomClusterRenderer(this.getApplicationContext(), mMap, mapClusterManager));
    }


    /**
     * Initialises the cluster manager with the context and the map,
     * points the map's listeners at the listeners implemented by the cluster manager and calls the addItems method.
     */
    private void clusterSetUp() {

        // Initialises the cluster manager with the context and the map.
        mapClusterManager = new ClusterManager<MyGoogleMapMarkers>(this, mMap);

        // Points the map's listeners at the listeners implemented by the cluster manager.
        mMap.setOnCameraIdleListener(mapClusterManager);
        // Add cluster items (markers) to the cluster manager.
        addItems();
    }

    /**
     * This method clears all the previous markers form the cluster manager before looping through the array list of restaurant objects and using the data to create markers.
     * The markers are then added to the cluster manager, before being clustered.
     */
    private void addItems() {

        // Clears all previous markers from the cluster manager object.
        mapClusterManager.getMarkerCollection().clear();

        // Loops through the array list of restaurants, pulls out the relevant information and adds the marker to the cluster manager.
        for (int i = 0; i < arrList.size(); i++) {

            /* Sets a slight offset to the latitude and longitude of all markers, to make sure there is no overlap of marker.
               This may happen if restaurants share the same building for example.
               The offset is calculated by generating a random number between 10 and 1 and dividing that number by 10000 to ensure only a slight change to the location*/
            double offsetLat = Math.floor((Math.random() * 10) + 1) / 10000;
            double offsetLng = Math.floor((Math.random() * 10) + 1) / 10000;

            // Obtains the longitude and latitude of the restaurant and adds the offset.
            lat = Double.parseDouble(arrList.get(i).getLatitude()) + offsetLat;
            lng = Double.parseDouble(arrList.get(i).getLongitude()) + offsetLng;

            // This BitmapDescriptor contains the resource location of the rating image that relates to the restaurant image.
            markerIcon = BitmapDescriptorFactory.fromResource(restRating.ratingMarkerImg(arrList.get(i).getRatingValue()));

            // a new marker object is created using the updated lat and long variables,
            // as well as the rating date passed in from the restaurant array list and finally the updated marker icon which represents the rating value of the restaurant.
            MyGoogleMapMarkers marker = new MyGoogleMapMarkers(lat, lng,
                    arrList.get(i).getBusinessName(), "Rating Date " + arrList.get(i).getRatingDate(), markerIcon);
            // The marker is the added to the cluster manager.
            mapClusterManager.addItem(marker);
        }
        // Once the loop has finished and all the markers have been added to the cluster manager, the cluster manager is re-clustered.
        mapClusterManager.cluster();

    }
}